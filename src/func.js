const getSum = (str1, str2) => {
  if (((typeof str1) !== "string") || ((typeof str2) !== "string")) {
    return false;
  }

  let a = str1 === "" ? "0" : str1;
  let b = str2 === "" ? "0" : str2;

  let padding = "";
  for (let i = 0; i < Math.abs(a.length - b.length); i++) {
    padding += "0";
  }

  if (a.length > b.length) {
    b = padding + b;
  } else if (b.length > a.length) {
    a = padding + a;
  }

  let result = "";
  let remain = 0;

  for (let i = 0; i < Math.min(a.length, b.length); i++) {
    const digitA = a[a.length - 1 - i];
    const digitB = b[b.length - 1 - i];

    if (digitA < "0" || digitA > "9" || digitB < "0" || digitB > "9") {
      return false;
    }

    result = (remain + parseInt(digitA) + parseInt(digitB)) % 10 + result;
    remain = Math.floor((parseInt(digitA) + parseInt(digitB)) / 10);
  }

  if (remain > 0) {
    result = remain + result;
  }

  return result;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;

  for (const post of listOfPosts) {
    if (post["author"] === authorName) {
      posts++;
    }

    if ("comments" in post) {
      for (const comment of post["comments"]) {
        if (comment["author"] === authorName) {
          comments++;
        }
      }
    }
  }

  return "Post:" + posts + ",comments:" + comments;
};

const tickets=(people)=> {
  const ticket = 25;
  let cashbox = 0;
  for (const person of people) {
    const change = person - ticket;
    if (change > cashbox) {
      return "NO";
    }
    cashbox += ticket;
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
